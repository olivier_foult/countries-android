package com.olivierfoult.countries

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView

class Helper {
    companion object {
        fun formatDouble(value: Double) : String {
            fun Double.format(digits: Int) = "%.${digits}f".format(this)
            return "${value.format(0)}"
        }

        fun addDividerToRecyclerView(viewId: Int, activity: Activity) {
            activity.findViewById<RecyclerView>(viewId).addItemDecoration(
                DividerItemDecoration(
                    activity,
                    DividerItemDecoration.VERTICAL
                )
            )
            val itemDecorator = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
            itemDecorator.setDrawable(ContextCompat.getDrawable(activity, R.drawable.divider)!!)
        }
    }


}