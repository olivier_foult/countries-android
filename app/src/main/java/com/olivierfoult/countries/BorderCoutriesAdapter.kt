package com.olivierfoult.countries

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.olivierfoult.countries.model.Country
import kotlinx.android.synthetic.main.country_layout.view.*

class BorderCountriesAdapter(val items : Array<Country>, val context: Context) : RecyclerView.Adapter<BorderViewHolder>() {
    override fun onBindViewHolder(holder: BorderViewHolder, position: Int) {
        val country = items[position]
        holder?.countryName?.text = country.nativeName + " - " + country.name + " (" + country.area + ")"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BorderViewHolder {
        return BorderViewHolder(LayoutInflater.from(context).inflate(R.layout.country_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class BorderViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val countryName = view.country_name
}
