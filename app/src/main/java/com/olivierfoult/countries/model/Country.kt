package com.olivierfoult.countries.model

import java.io.Serializable

class Country : Serializable {
    var name: String = ""
    var nativeName: String = ""
    var alpha3Code: String = ""
    var area : Double = Double.MIN_VALUE
    var borders: ArrayList<String> = ArrayList()
}
