package com.olivierfoult.countries

import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.olivierfoult.countries.model.Country
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    companion object {
        var countries: Array<Country> = arrayOf<Country>()
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getCountries()
    }

    private fun initiateList() {
        viewManager = LinearLayoutManager(this)
        viewAdapter = CountriesAdapter(countries, this)
        Helper.addDividerToRecyclerView(R.id.countries_list, this)

        countries_list.layoutManager = viewManager
        countries_list.adapter = viewAdapter

    }

    private fun getCountries() {
        Fuel.get("https://restcountries.eu/rest/v2/all")
            .responseObject(CountriesDataDeserializer()) { request, response, result ->
                val (countries, err) = result
                if (err != null) {
                    Log.e("API", "error fetching the countries: $err")
                    countries_error_text.text = "Error fetching the countries: ${err.message}"
                    countries_error.visibility = View.VISIBLE
                    countries_list.visibility = View.GONE
                    sort.visibility = View.GONE
                } else {
                    countries_error.visibility = View.GONE
                    countries_list.visibility = View.VISIBLE
                    sort.visibility = View.VISIBLE
                    MainActivity.countries = countries!!
                    this.initiateList()
                }
            }
    }

    fun retry(view: View) {
        getCountries()
    }

    fun sortByNameDesc(view: View) {
        countries.sortWith(compareByDescending { it.name })
        viewAdapter.notifyDataSetChanged()
    }
    fun sortByNameAsc(view: View) {
        countries.sortWith(compareBy { it.name })
        viewAdapter.notifyDataSetChanged()
    }

    fun sortByPopulationDesc(view: View) {
        countries.sortWith(compareByDescending { it.area })
        viewAdapter.notifyDataSetChanged()
    }
    fun sortByPopulationAsc(view: View) {
        countries.sortWith(compareBy { it.area })
        viewAdapter.notifyDataSetChanged()
    }


    class CountriesDataDeserializer : ResponseDeserializable<Array<Country>> {
        override fun deserialize(content: String): Array<Country> = Gson().fromJson(content, Array<Country>::class.java)
    }

}
