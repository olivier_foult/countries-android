package com.olivierfoult.countries

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.olivierfoult.countries.model.Country
import kotlinx.android.synthetic.main.country_layout.view.*

class CountriesAdapter(val items : Array<Country>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val country = items[position]
        holder?.countryName?.text = country.nativeName + " - " + country.name + " (" + Helper.formatDouble(country.area) + ")"
        holder?.mainView.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, CountryDetailsActivity::class.java)
            intent.putExtra(CountryDetailsActivity.EXTRA_COUNTRY, country)
            ContextCompat.startActivity(context, intent, null)
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.country_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val countryName = view.country_name
    var mainView = view
}
