package com.olivierfoult.countries

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.olivierfoult.countries.model.Country
import kotlinx.android.synthetic.main.activity_country_details.*

class CountryDetailsActivity : AppCompatActivity() {
    private var country: Country = Country()

    companion object {
        const val EXTRA_COUNTRY: String = "EXTRA_COUNTRY"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_country_details)

        country = intent.getSerializableExtra(EXTRA_COUNTRY) as Country
        initializeActionBar()
        initializeList()
    }

    private fun initializeActionBar() {
        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "Borders countries of " + country.name + " - " + country.nativeName
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    private fun initializeList() {
        val borderCountries = MainActivity.countries.filter {
            country.borders.contains(it.alpha3Code)
        }
        if (borderCountries.isEmpty()) {
            no_border_countries.visibility = View.VISIBLE
        } else {
            Helper.addDividerToRecyclerView(R.id.bordered_countries_list, this)
        }


        bordered_countries_list.layoutManager = LinearLayoutManager(this)
        bordered_countries_list.adapter = BorderCountriesAdapter(borderCountries.toTypedArray(), this)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
